from django.shortcuts import render_to_response
from django.template import RequestContext

def home(request, template_name="index.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def clasificados(request, template_name="clasificados.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def nuevo_clasificado(request, template_name="clasificado_nuevo.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def veterinarias(request, template_name="veterinarias.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def adopcion(request, template_name="adopcion.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def adoptar(request, template_name="adoptar.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def busqueda(request, template_name="busqueda.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def hallazgo(request, template_name="hallazgo.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def cruza(request, template_name="cruza.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def nueva_cruza(request, template_name="cruza_nueva.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def perfil(request, template_name="mis_anuncios.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def mensajes(request, template_name="mensajes.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))

def signin(request, template_name="signin.html"):
    return render_to_response(template_name,
            context_instance=RequestContext(request))

def signup(request, template_name="signup.html"):
    return render_to_response(template_name,
            context_instance=RequestContext(request))

def veterinarias(request, template_name="veterinarias.html"):
    return render_to_response(template_name,
        context_instance=RequestContext(request))
