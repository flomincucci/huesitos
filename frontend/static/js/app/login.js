function LoginViewModel() {
    var self = this;
    self.loginURI = '/api/users/?format=json';

    self.username = ko.observable();
    self.password = ko.observable();

    self.ajax = function(uri, method, data) {
        var request = {
            url: uri,
            type: method,
            contentType: "application/json",
            cache: false,
            dataType: 'json',
            data: JSON.stringify(data),
            error: function(jqXHR) {
                console.log("ajax error " + jqXHR.status);
            }
        };
        return $.ajax(request);
    };

    self.click_login = function() {
       self.ajax(self.loginURI + '&username=' + self.username() 
           + '&password=' + self.password(), 'GET').done(function(data) {
            localStorage.setItem('user', data[0]['id']);
            window.location.href='/'
       });
    };
}

ko.applyBindings(new LoginViewModel());
