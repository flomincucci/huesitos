function ClasificadoViewModel() {
    var self = this;
    self.clasificadosURI = '/api/clasificados/?format=json';
    self.catsURI = '/api/clasificados/categorias/?format=json';
    self.clasificados = ko.observableArray();
    self.categorias = ko.observableArray();

    self.ajax = function(uri, method, data) {
        var request = {
            url: uri,
            type: method,
            contentType: "application/json",
            cache: false,
            dataType: 'json',
            data: JSON.stringify(data),
            error: function(jqXHR) {
                console.log("ajax error " + jqXHR.status);
            }
        };
        return $.ajax(request);
    }

    self.imagen_mapa = ko.observable();
    self.categoria_activa = ko.observable(0);

    self.ajax(self.clasificadosURI, 'GET').done(function(data) {
        var clas = data.clasificados
        for (var i = 0; i < clas.length; i++) {
            self.clasificados.push({
                id_usuario: ko.observable(clas[i].id_usuario),
                id_categoria: ko.observable(clas[i].id_categoria),
                categoria: ko.observable(clas[i].categoria.nombre),
                ubicacion: ko.observableArray([{
                    lat: ko.observable(clas[i].ubicacion.lat),
                    lon: ko.observable(clas[i].ubicacion.lon)}]),
                precio: ko.observable(clas[i].precio),
                fecha: ko.observable(clas[i].fecha),
                descripcion: ko.observable(clas[i].descripcion)
            });
        }
    });

    self.ajax(self.catsURI, 'GET').done(function(data) {
        for (var i = 0; i < data.categories.length; i++) {
            self.categorias.push({
                id: ko.observable(data.categories[i].id),
                nombre: ko.observable(data.categories[i].nombre)
            });
        };
    });

    self.filtrar_avisos = function(categoria) {
        self.categoria_activa(categoria.id());
    }

    self.limpiar_filtro = function() {
        self.categoria_activa(0);
    }

    self.ver_mapa = function(clas) {
        self.imagen_mapa('<img src="https://maps.googleapis.com/maps/api/staticmap?&size=400x400&key=AIzaSyALXvVhB-CXLa68DmJTCt022FUxYxgSFkk&&markers=color:green%7C'+ clas.ubicacion()[0].lat() +','+ clas.ubicacion()[0].lon() +'&zoom=15" />'); 
        $("#modal").modalBox({
            keyClose: true,
            bodyClose: true,
            overlay: true
            });
    }
}

ko.applyBindings(new ClasificadoViewModel());

