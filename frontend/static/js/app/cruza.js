function CruzaViewModel() {
    var self = this;
    self.cruzaURI = '/api/cruza/?format=json';
    self.razaURI = '/api/razas/?format=json';

    self.ajax = function(uri, method, data) {
        var request = {
            url: uri,
            type: method,
            contentType: "application/json",
            cache: false,
            dataType: 'json',
            data: JSON.stringify(data),
            error: function(jqXHR) {
                console.log("ajax error " + jqXHR.status);
            }
        };
        return $.ajax(request);
    }

    var unaOpcion = function(id, nombre) {
        this.id = id;
        this.nombre = nombre;
    };

    //Valores
    self.latitud = ko.observable();
    self.longitud= ko.observable();
    self.ubicacion = ko.computed(function(){
        return {lat: self.latitud(), lon: self.longitud()};
    });
    self.raza = ko.observableArray(); 
    self.opciones_color = ko.observableArray([
        new unaOpcion("C1","Blanco"),
        new unaOpcion("C2","Negro"),
        new unaOpcion("C3","Gris"),
        new unaOpcion("C4","Marron-Arena"),
        new unaOpcion("C5","Blanco y negro"),
        new unaOpcion("C6","Blanco y marron"),
        new unaOpcion("C7","Blanco y gris"),
        new unaOpcion("C8","Negro-marron"),
        new unaOpcion("C9","Negro-gris"),
        new unaOpcion("C10","Gris y marron"),
    ]);
    self.opciones_tamanio = ko.observableArray([
        new unaOpcion("C", "Chico"),
        new unaOpcion("M", "Mediano"),
        new unaOpcion("G", "Grande"),
    ]);
    self.opciones_sexo = ko.observableArray([
        new unaOpcion("M", "Macho"),
        new unaOpcion("H", "Hembra"),
    ]);    
    self.opciones_edad = ko.observableArray([
        new unaOpcion("C", "Cachorro"),
        new unaOpcion("A", "Adulto"),
        new unaOpcion("v", "Anciano"),
    ]);      

    //Campos
    self.id_usuario = ko.observable(localStorage.getItem("user"));
    self.ubicacion = ko.pureComputed(function() {
            return {"lat": self.latitud, "lon": self.longitud}
        }, this);
    self.id_raza = ko.observable(); 
    self.color = ko.observable(); 
    self.tamanio = ko.observable(); 
    self.sexo = ko.observable(); 
    self.edad = ko.observable(); 
    self.papeles = ko.observable(false);
    self.libreta = ko.observable(false);
    self.perros_fotos = ko.observableArray();
    self.perros_seleccionados = ko.computed(function() {
        return ko.utils.arrayFilter(self.perros_fotos(), function(foto) { return foto.estaSeleccionada() });
    });
    

    //Ajax
    self.ajax(self.razaURI, 'GET').done(function(data) {
            for (var i = 0; i < data.razas.length; i++) {
                self.raza.push(
                    new unaOpcion(data.razas[i].id, data.razas[i].nombre)
                );
            }
    });
    
    //Botones
    self.buscar = function(){
        self.ajax(self.cruzaURI, 'GET').done(function(data) {
            var hal = data.avisos;
            var mapped_perros = $.map(hal, function(item) { return new Foto(item) });
            self.perros_fotos(mapped_perros);
        });
        $("#paso-1").hide();
        $("#paso-2").show();
    };

    self.click_perro = function(perro) {
        if (perro.estaSeleccionada()) {
            perro.estaSeleccionada(false);
        } else {
            perro.estaSeleccionada(true);
        }
    };

   self.ver_fichas = function() {
        $("#paso-2").hide();
        $("#paso-3").show();
   } 

};

function Foto(data) {
    this.id = ko.observable(data.id);
    this.imagen_perro = ko.observable('<img width="200" height="200" src="/api/imagenes/hallazgo/' + data.fotos[0] + '" />');
    this.ubicacion = ko.observableArray([data.ubicacion]);
    this.ubicacion = ko.observableArray([data.ubicacion]);
    this.nombre = ko.observableArray([data.nombre]);
    this.id_usuario = ko.observableArray([data.id_usuario]);
    this.color = ko.observableArray([data.color]);
    this.tamanio = ko.observableArray([data.tamanio]);
    this.edad = ko.observableArray([data.edad]);
    this.sexo = ko.observableArray([data.sexo]);
    this.papeles = ko.observableArray([data.papeles]);
    this.libreta = ko.observableArray([data.libreta]);
    this.estaSeleccionada = ko.observable();
}

function actualizar_marcador(latitud,longitud) {
    if (formmap.markers.length > 0) {
        formmap.markers[0].setPosition(new google.maps.LatLng(latitud, longitud))
    } else {
        formmap.addMarker({
            lat: latitud,
            lng: longitud,
        });
    }
    $("#ub-lat").val(latitud).change();
    $("#ub-lon").val(longitud).change();
}

ko.applyBindings(new CruzaViewModel());

