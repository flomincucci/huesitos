function NewClasificadoViewModel() {
    var self = this;
    self.clasificadosURI = '/api/clasificados/';
    self.catsURI = '/api/clasificados/categorias/?format=json';

    self.ajax = function(uri, method, data) {
        var request = {
            url: uri,
            type: method,
            contentType: "application/json",
            cache: false,
            dataType: 'json',
            data: data,
            error: function(jqXHR) {
                console.log("ajax error " + jqXHR.status);
            }
        };
        return $.ajax(request);
    }

    var unaOpcion = function(id, nombre) {
        this.id = id;
        this.nombre = nombre;
    };

    self.id_usuario = ko.observable(localStorage.getItem("user"));
    self.id_categoria = ko.observable();
    self.categorias = ko.observableArray();
    self.precio = ko.observable();
    self.fecha = ko.observable();
    self.descripcion = ko.observable();
    self.latitud = ko.observable();
    self.longitud = ko.observable();
    self.fecha = ko.pureComputed(function() {
        return moment().format("YYYY-MM-DD");
        }, this);
    self.ubicacion = ko.pureComputed(function() {
            return {"lat": self.latitud, "lon": self.longitud}
        }, this);

    self.ajax(self.catsURI, 'GET').done(function(data) {
        for (var i = 0; i < data.categories.length; i++) {
            self.categorias.push(
                new unaOpcion(data.categories[i].id, data.categories[i].nombre)
            );
        }
    });

    self.publicar = function() {
        var datos = ko.toJSON(this);
        self.ajax(self.clasificadosURI, 'POST', datos).done(
            function() {
                alert("Publicado!");
                history.go(-1);
            }
        );
    }

}

function actualizar_marcador(latitud,longitud) {
    if (formmap.markers.length > 0) {
        formmap.markers[0].setPosition(new google.maps.LatLng(latitud, longitud))
    } else {
        formmap.addMarker({
            lat: latitud,
            lng: longitud,
        });
    }
    $("#ub-lat").val(latitud).change();
    $("#ub-lon").val(longitud).change();
}


ko.applyBindings(new NewClasificadoViewModel());

