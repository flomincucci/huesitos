function loadResults (data) {
  var items, markers_data = [];
  if (data.veterinarias.length > 0) {
    items = data.veterinarias;

    for (var i = 0; i < items.length; i++) {
      var item = items[i];

        markers_data.push({
          lat : item.ubicacion.lat,
          lng : item.ubicacion.lon,
          title : item.nombre,
          puntaje: item.puntaje,
          icon: {
              url: "http://i.imgur.com/12vESUh.png",
              size: new google.maps.Size(24, 24),
          },
          infoWindow: {
              content: "<p><strong>" + item.nombre + "</strong><br/>Direccion: " + item.nombre_ubicacion + "<br/>Horario: " + item.horario + "<br />Puntaje: " + item.puntaje + "</p>" 
          }
        });
    }
  }

  map.addMarkers(markers_data);
}

function puntuar() {
    $("#modal").show();
}

function enviar_puntuacion() {
    $("#modal").hide();
}

$.fn.stars = function() {
    return $(this).each(function() {
        $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 16));
    });
}

$(document).on('click', '.pan-to-marker', function(e) {
  e.preventDefault();
  var position, lat, lng, $index;
  $index = $(this).data('marker-index');
  position = map.markers[$index].getPosition();
  lat = position.lat();
  lng = position.lng();
  map.setCenter(lat, lng);
});

function cargar_veterinarias(){
  map.on('marker_added', function (marker) {
    var index = map.markers.indexOf(marker);
    if (index > 0) {
    $('#resultados').append('<tr><td>' + index  + '</td><td><a href="#" class="pan-to-marker" data-marker-index="' + index + '">' + marker.title + '</a></td><td><span class="stars"> ' + marker.puntaje + '</span></td><td><button type="button" class="btn btn-default btn-xs" onclick="puntuar()">Puntuar</button></td></tr>');
  }

    if (index == map.markers.length - 1) {
      map.fitZoom();
    }
  });

  var xhr = $.getJSON('/api/veterinarias');
  xhr.done(loadResults);
}
    
