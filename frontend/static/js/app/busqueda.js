function BusquedaViewModel() {
    var self = this;
    self.busquedaURI = '/api/hallazgos/?format=json';
    self.razaURI = '/api/razas/?format=json';

    self.ajax = function(uri, method, data) {
        var request = {
            url: uri,
            type: method,
            contentType: "application/json",
            cache: false,
            dataType: 'json',
            data: JSON.stringify(data),
            error: function(jqXHR) {
                console.log("ajax error " + jqXHR.status);
            }
        };
        return $.ajax(request);
    }

    var unaOpcion = function(id, nombre) {
        this.id = id;
        this.nombre = nombre;
    };

    //Campos
    self.latitud = ko.observable();
    self.longitud= ko.observable();
    self.ubicacion = ko.computed(function(){
        return {lat: self.latitud(), lon: self.longitud()};
    });
    self.raza = ko.observableArray(); 
    self.color = ko.observableArray([
        new unaOpcion("C1","Blanco"),
        new unaOpcion("C2","Negro"),
        new unaOpcion("C3","Gris"),
        new unaOpcion("C4","Marron-Arena"),
        new unaOpcion("C5","Blanco y negro"),
        new unaOpcion("C6","Blanco y marron"),
        new unaOpcion("C7","Blanco y gris"),
        new unaOpcion("C8","Negro-marron"),
        new unaOpcion("C9","Negro-gris"),
        new unaOpcion("C10","Gris y marron"),
    ]);
    self.tamanio = ko.observableArray([
        new unaOpcion("C", "Chico"),
        new unaOpcion("M", "Mediano"),
        new unaOpcion("G", "Grande"),
    ]);
    self.perros_fotos = ko.observableArray();
    self.mapa = ko.observableArray();
    self.fotos_seleccionadas = ko.computed(function() {
        return ko.utils.arrayFilter(self.perros_fotos(), function(foto) { return foto.estaSeleccionada() });
    });

    //Ajax
    self.ajax(self.razaURI, 'GET').done(function(data) {
            for (var i = 0; i < data.razas.length; i++) {
                self.raza.push(
                    new unaOpcion(data.razas[i].id, data.razas[i].nombre)
                );
            }
    });
    
    //Botones
    self.buscar = function(){
        self.ajax(self.busquedaURI, 'GET').done(function(data) {
            var hal = data.hallazgos;
            var mapped_perros = $.map(hal, function(item) { return new Foto(item) });
            self.perros_fotos(mapped_perros);
        });
        $("#paso-1").hide();
        $("#paso-2").show();
    };

    self.click_perro = function(perro) {
        if (perro.estaSeleccionada()) {
            perro.estaSeleccionada(false);
        } else {
            perro.estaSeleccionada(true);
        }
    };

    self.cargar_mapa = function(){
        map.removeMarkers();
        var perros = self.fotos_seleccionadas()

        for(var m = 0; m < perros.length; m++){
            ubicacion = perros[m].ubicacion();
            imagen = perros[m].imagen_perro();
            var color = "FE7569";
            if (m == perros.length - 1) {
              color =  "32CD32"
            }
            map.addMarker({
                lat: ubicacion[0].lat,
                lng: ubicacion[0].lon,
                icon: { 
                  url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + color,
                  size : new google.maps.Size(40, 40)
                },
                infoWindow: {
                    content: imagen 
                }
            });
        };
        $("#paso-2").hide();
        $("#paso-3").show();
    };

    self.ver_recorrido = function() {
        var perros = self.fotos_seleccionadas();
        var ubicaciones_perros = [];
        for (var u = 0; u < perros.length; u++){
            if (u == 0 || u == (perros.length - 1)) {
                ubicaciones_perros.push([perros[u].ubicacion()[0].lat, perros[u].ubicacion()[0].lon]); 
            } else {
                ubicaciones_perros.push({location: new google.maps.LatLng(perros[u].ubicacion()[0].lat, perros[u].ubicacion()[0].lon)}); 
            }
        }
        var primero = ubicaciones_perros.shift();
        var ultimo = ubicaciones_perros.pop();
        map.drawRoute({
            origin: primero,
            destination: ultimo,
            waypoints: ubicaciones_perros
        });
    };
};

function Foto(data) {
    this.id = ko.observable(data.id);
    this.imagen_perro = ko.observable('<img width="200" height="200" src="/api/imagenes/hallazgo/' + data.fotos[0] + '" />');
    this.ubicacion = ko.observableArray([data.ubicacion]);
    this.estaSeleccionada = ko.observable();
}

function actualizar_marcador(latitud,longitud) {
    if (formmap.markers.length > 0) {
        formmap.markers[0].setPosition(new google.maps.LatLng(latitud, longitud))
    } else {
        formmap.addMarker({
            lat: latitud,
            lng: longitud,
            icon: { 
              url: "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FE7569",
              size: new google.maps.Size(30, 30)
            },
        });
    }
    $("#ub-lat").val(latitud).change();
    $("#ub-lon").val(longitud).change();
}

ko.applyBindings(new BusquedaViewModel());

