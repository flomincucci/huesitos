function MensajeViewModel() {
    var self = this;
    self.user = localStorage.getItem('user');

    if(!self.user) {
      //TODO: Feedback. Si no esta logeado no deberia acceder
      window.location.href = '/';
    }

    self.mensajesURI = '/api/user/' + self.user + '/mensajes/'; 
    self.mensajes = ko.observableArray();


    // Parametros del mensaje a enviar
    self.titulo = ko.observable();
    self.mensaje = ko.observable();
    self.destino = ko.observable();

    self.ajax = function(uri, method, data) {
        var request = {
            url: uri,
            type: method,
            contentType: "application/json",
            cache: false,
            dataType: 'json',
            data: JSON.stringify(data),
            error: function(jqXHR) {
                console.log("ajax error " + jqXHR.status);
            }
        };
        return $.ajax(request);
    }

    self.reload = function() {
      self.ajax(self.mensajesURI, 'GET').done(function(data) {
        self.mensajes(data.mensajes);
      });
    };

    self.reload();

    self.responder = function(mensaje) {
      //TODO: Aca habria que cambiar la property de display de los elementos
      // y mostrar solo el formulario de envio de mensaje
      self.destino(mensaje.usuario_origen);
      $("#modal").show();
    };

    self.eliminar_mensaje = function(mensaje) {
      self.ajax('/api/mensaje/' + mensaje.id + '/', 'DELETE').done(function(response) {
        self.reload();
      });
    };

    self.enviar = function() {
      data = {};
      data.usuario_origen = self.user;
      data.usuario_destino = self.destino();
      data.mensaje = self.mensaje();
      data.titulo = self.titulo();
      data.fecha = "2014-10-10T20:00:00";
      data.reference_id = "0";
      data.modulo = "O";

      console.log(JSON.stringify(data));

      self.ajax('/api/mensaje/', 'POST', data).done(function(response) {
        self.titulo("");
        self.mensaje("");
        self.reload();
        $("#modal").hide();
      });
    };
};

function Mensaje(data) {
  this.usuario_origen = data.usuario_origen;
  this.usuario_destino = data.usuario_destino;
  this.mensaje = data.mensaje;
  this.fecha = "2014-10-10T20:00:00";
  this.reference_id = 0;
}

ko.applyBindings(new MensajeViewModel());
