function AvisosViewModel() {
        var self = this;
        self.avisosURI = '/api/user/1/adopcion';
        self.aviso = ko.observableArray();

        self.ajax = function(uri, method, data) {
            var request = {
                url: uri,
                type: method,
                contentType: "application/json",
                accepts: "application/json",
                cache: false,
                dataType: 'json',
                data: JSON.stringify(data),
                error: function(jqXHR) {
                    console.log("ajax error " + jqXHR.status);
                }
            };
            return $.ajax(request);
        }

        self.aviso([
            {
                nombre: ko.observable('title #1'),
                solicitud: ko.observableArray(
                    [{
                        nombre: ko.observable('Pedro55')
                     }])
            },
            {
                nombre: ko.observable('title #2'),
                solicitud: ko.observableArray(
                    [{
                        nombre: ko.observable('Pedro5')
                     }])
            }
        ]);

        self.editar_aviso = function(aviso) {
            alert("Edit: " + aviso.nombre());
        }
        self.eliminar_aviso = function(aviso) {
            alert("Remove: " + aviso.nombre());
        }

        self.ajax(self.avisosURI, 'GET').done(function(data) {
                for (var i = 0; i < data.avisos.length; i++) {
                    self.aviso.push({
                        nombre: ko.observable(data.avisos[i].nombre),
                    });
                }
        });

    }

ko.applyBindings(new AvisosViewModel(), $('.tab-content')[0]);

