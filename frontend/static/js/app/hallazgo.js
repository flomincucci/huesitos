function HallazgoViewModel() {
    var self = this;
    self.hallazgosURI = '/api/hallazgos/';
    self.razaURI = '/api/razas/?format=json';

    self.ajax = function(uri, method, data) {
        var request = {
            url: uri,
            type: method,
            contentType: "application/json",
            cache: false,
            dataType: 'json',
            data: data,
            error: function(jqXHR) {
                console.log("ajax error " + jqXHR.status);
            }
        };
        return $.ajax(request);
    }

    var unaOpcion = function(id, nombre) {
        this.id = id;
        this.nombre = nombre;
    };

    //Valores
    self.latitud = ko.observable();
    self.longitud= ko.observable();
    self.raza = ko.observableArray(); 
    self.opciones_color = ko.observableArray([
        new unaOpcion("C1","Blanco"),
        new unaOpcion("C2","Negro"),
        new unaOpcion("C3","Gris"),
        new unaOpcion("C4","Marron-Arena"),
        new unaOpcion("C5","Blanco y negro"),
        new unaOpcion("C6","Blanco y marron"),
        new unaOpcion("C7","Blanco y gris"),
        new unaOpcion("C8","Negro-marron"),
        new unaOpcion("C9","Negro-gris"),
        new unaOpcion("C10","Gris y marron"),
    ]);
    self.opciones_tamanio = ko.observableArray([
        new unaOpcion("C", "Chico"),
        new unaOpcion("M", "Mediano"),
        new unaOpcion("G", "Grande"),
    ]);
    self.opciones_sexo = ko.observableArray([
        new unaOpcion("M", "Macho"),
        new unaOpcion("H", "Hembra"),
    ]);    
    self.opciones_edad = ko.observableArray([
        new unaOpcion("C", "Cachorro"),
        new unaOpcion("A", "Adulto"),
        new unaOpcion("v", "Anciano"),
    ]);      

    //Campos
    self.id_usuario = ko.observable(localStorage.getItem("user"));
    self.ubicacion = ko.pureComputed(function() {
            return {"lat": self.latitud, "lon": self.longitud}
        }, this);
    self.id_raza = ko.observable(); 
    self.color = ko.observable(); 
    self.tamanio = ko.observable(); 
    self.sexo = ko.observable(); 
    self.edad = ko.observable(); 
    self.nombre = ko.observable("Perro");
    self.id = ko.observable();
    

    //Ajax
    self.ajax(self.razaURI, 'GET').done(function(data) {
            for (var i = 0; i < data.razas.length; i++) {
                self.raza.push(
                    new unaOpcion(data.razas[i].id, data.razas[i].nombre)
                );
            }
    });
    
    //Botones
    self.publicar = function(){
        var datos = ko.toJSON(this);
        self.ajax(self.hallazgosURI, 'POST', datos).done(
            function(data) {
                self.id(data.id);
            }
        );
        $("#paso-1").hide();
        $("#paso-2").show();
    };

    self.subir_fotos = function(){
            var fd = new FormData($(".form-imagenes")[0]);
            $.ajax({
                url: '/api/imagenes/hallazgo/',
                type: 'POST',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                success: function() {
                    window.location = '/';
                }
            });
    };


    //Funciones de imagenes
    var slotModel = function() {
        var that = {};
        that.imageFile = ko.observable();
        that.imageObjectURL = ko.observable();
        that.imageBinary = ko.observable();
        return that;
    };
    self.imagenes = ko.observableArray();
    self.beforeRemoveSlot = function(element, index, data) {
        if (data.imageObjectURL()) {
            windowURL.revokeObjectURL(data.imageObjectURL());
        }
        $(element).remove();
    };
    self.addSlot = function() {
        self.imagenes.push(slotModel());
    };
    self.removeSlot = function(data) {
        self.imagenes.remove(data);
    };

};

function actualizar_marcador(latitud,longitud) {
    if (formmap.markers.length > 0) {
        formmap.markers[0].setPosition(new google.maps.LatLng(latitud, longitud))
    } else {
        formmap.addMarker({
            lat: latitud,
            lng: longitud,
        });
    }
    $("#ub-lat").val(latitud).change();
    $("#ub-lon").val(longitud).change();
}

ko.applyBindings(new HallazgoViewModel());
