from django.contrib import admin
from backend.models import *

# Register your models here.
admin.site.register(CategoriasClasificado)
admin.site.register(Raza)
admin.site.register(Busqueda)
admin.site.register(Ubicacion)
admin.site.register(Hallazgo)
admin.site.register(Cruza)
admin.site.register(SolicitudCruza)
admin.site.register(Adopcion)
admin.site.register(SolicitudAdopcion)
admin.site.register(Veterinaria)
admin.site.register(Calificacion)
admin.site.register(Clasificado)
admin.site.register(Mensaje)
