from django.conf import settings
from django.db import models

class Usuario(models.Model):
    password = models.CharField(max_length=300)
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)
    username = models.CharField(max_length=300)
    email = models.CharField(max_length=300)

    def __unicode__(self):
        return self.first_name


class Ubicacion(models.Model):
    lat = models.FloatField()
    lon = models.FloatField()

    def __unicode__(self):
        return str(self.lat) + "," + str(self.lon)

"""
OPCIONES_RAZAS = (
        ('AM', 'Alaskan Malamute'),
        ('BE', 'Beagle'),
        ('BI', 'Bichon Frise'),
        ('CA', 'Caniche'),
        ('CH', 'Chihuahua'),
    )
"""
class Raza(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300, null = True)

    def __unicode__(self):
        return self.nombre

class Perro(models.Model):
    OPCIONES_TAMANIO = (
        ('D', 'Desconoce'),
        ('C', 'Chico'),
        ('M', 'Mediano'),
        ('G', 'Grande'),
    )
    OPCIONES_COLOR = (
        ('D', 'Desconoce'),
        ('C1', 'Blanco'),
        ('C2', 'Negro'),
        ('C3', 'Gris'),
        ('C4', 'Marron-Arena'),
        ('C5', 'Blanco y negro'),
        ('C6', 'Blanco y marron'),
        ('C7', 'Blanco y gris'),
        ('C8', 'Negro y marron'),
        ('C9', 'Negro y gris'),
        ('C10', 'Gris y marron'),
    )
    OPCIONES_SEXO = (
        ('D', 'Desconoce'),
        ('M', 'Masculino'),
        ('F', 'Femenino'),
    )
    OPCIONES_EDAD = (
        ('D', 'Desconoce'),
        ('C', 'Cachorro'),
        ('A', 'Adulto'),
        ('V', 'Anciano')
    )

    nombre = models.CharField(max_length=100)
    raza = models.ForeignKey(Raza)
    edad = models.CharField(max_length=1,
                            choices=OPCIONES_EDAD,
                            default='M')
    tamanio = models.CharField(max_length=1,
                            choices=OPCIONES_TAMANIO,
                            default='M')
    color = models.CharField(max_length=2,
                            choices=OPCIONES_COLOR,
                            default='C1')
    sexo = models.CharField(max_length=1,
                            choices=OPCIONES_SEXO,
                            default='M')
    usuario = models.ForeignKey(Usuario)
    ubicacion = models.ForeignKey(Ubicacion)

    class Meta:
        abstract = True


class FotoPerro(models.Model):
    imagen = models.ImageField(upload_to='images')
    descripcion = models.CharField(max_length=300, null = True)
    def image_tag(self):
        return u'<img src="%s" />' % self.imagen.path
    image_tag.short_description = 'Image'
    image_tag.allow_tags = True
    def __unicode__(self):
        return str(self.id)

    class Meta:
        abstract = True

class Busqueda(Perro):
    ESTADOS_BUSQUEDA = (
        ('A', 'Activa'),
        ('I', 'Inactiva'),
    )

    notificaciones = models.BooleanField(default=True)
    estado = models.CharField(max_length=1,
                             choices=ESTADOS_BUSQUEDA,
                             default='A')
    def __unicode__(self):
         return "%s (%s)" % (self.nombre, self.estado)

    def darDeBaja(self):
        self.estado = 'I'
        return self

class Hallazgo(Perro):
    pass

class FotoHallazgo(FotoPerro):
    hallazgo = models.ForeignKey(Hallazgo, related_name='fotos')

class Cruza(Perro):
    solicitudes = models.ManyToManyField('self', through='SolicitudCruza', symmetrical=False)
    papeles = models.BooleanField()
    libreta = models.BooleanField()

    def __unicode__(self):
        return self.nombre

class FotoCruza(FotoPerro):
    cruza = models.ForeignKey(Cruza, related_name='fotos')

class SolicitudCruza(models.Model):
    OPCIONES_ESTADO = (
        ('P', 'Pendiente'),
        ('A', 'Aceptada'),
        ('R', 'Rechazada'),
    )

    solicitante = models.ForeignKey(Cruza, related_name='solicitante')
    solicitado = models.ForeignKey(Cruza, related_name='solicitado')
    estado = models.CharField(max_length=1,
                            choices=OPCIONES_ESTADO,
                            default='P')

    def __unicode__(self):
        return "(%s) -> (%s)" % (self.solicitante, self.solicitado)

class Adopcion(Perro):
    solicitudes = models.ManyToManyField(Usuario, through='SolicitudAdopcion', related_name='solicitudes_adopcion')
    papeles = models.BooleanField()
    libreta = models.BooleanField()

    def __unicode__(self):
        return self.nombre

class SolicitudAdopcion(models.Model):
    OPCIONES_ESTADO = (
        ('P', 'Pendiente'),
        ('A', 'Aceptada'),
        ('R', 'Rechazada'),
    )

    solicitante = models.ForeignKey(Usuario, related_name='solicitante')
    solicitado = models.ForeignKey(Adopcion, related_name='solicitado')
    estado = models.CharField(max_length=1,
                            choices=OPCIONES_ESTADO,
                            default='P')

    def __unicode__(self):
        return "(%s) -> (%s)" % (self.solicitante, self.solicitado)

class FotoAdopcion(FotoPerro):
    adopcion = models.ForeignKey(Adopcion, related_name='fotos')

class Veterinaria(models.Model):
    nombre = models.CharField(max_length=100)
    puntaje = models.FloatField()
    horario = models.CharField(max_length=50)
    nombre_ubicacion = models.CharField(max_length=100)
    ubicacion = models.ForeignKey(Ubicacion)

    def __unicode__(self):
        return self.nombre

class Calificacion(models.Model):
    usuario = models.ForeignKey(Usuario)
    veterinaria = models.ForeignKey(Veterinaria)
    puntaje = models.IntegerField()
    comentario = models.CharField(max_length=150)

"""
OPCIONES_CATEGORIA = (
        ('PA', 'Paseador'),
        ('PE', 'Peluqueria'),
        ('AD', 'Adiestrador'),
    )
"""
class CategoriasClasificado(models.Model):
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300)

    def __unicode__(self):
        return self.nombre

class Clasificado(models.Model):
    usuario = models.ForeignKey(Usuario)
    categoria = models.ForeignKey(CategoriasClasificado)
    ubicacion = models.ForeignKey(Ubicacion)
    precio = models.IntegerField(default=0)
    fecha = models.DateField()
    descripcion = models.CharField(max_length=300)

    def __unicode__(self):
        return self.descripcion

class Foto(models.Model):
    imagen = models.ImageField(upload_to='images')
    descripcion = models.CharField(max_length=300)
    def image_tag(self):
        return u'<img src="%s" />' % self.imagen.path
    image_tag.short_description = 'Image'
    image_tag.allow_tags = True

class Mensaje(models.Model):
    OPCIONES_MODULO = (
        ('A', 'Adopcion'),
        ('C', 'Cruza'),
        ('H', 'Hallazgo'),
        ('O', 'Otro')
    )
    titulo = models.CharField(max_length=300)
    mensaje = models.CharField(max_length=300)
    modulo = models.CharField(max_length=1,
                            choices=OPCIONES_MODULO,
                            default='P')
    usuario_origen = models.ForeignKey(Usuario, related_name='usuario_origen')
    usuario_destino = models.ForeignKey(Usuario, related_name='usuario_destino')
    fecha = models.DateTimeField(auto_now_add=True, blank=True)
    leido = models.BooleanField(default = False)
    reference_id = models.IntegerField(default = 0)

    def __unicode__(self):
        return self.titulo

