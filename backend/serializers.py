from rest_framework import serializers
from backend.models import *
from django.contrib.auth.models import User


class UbicacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ubicacion
        fields = ('lat', 'lon')

class RazaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Raza
        fields = ('id', 'nombre', 'descripcion')

# Serializador para clasificados y sus categorias
class CategClasifSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoriasClasificado
        fields = ('id', 'nombre')

class ClasifSerializer(serializers.HyperlinkedModelSerializer):
    ubicacion = UbicacionSerializer()
    id_usuario = serializers.CharField(source='usuario_id')
    id_categoria = serializers.CharField(source='categoria_id')
    categoria = CategClasifSerializer(required=False)
    class Meta:
        model = Clasificado
        fields = ('id', 'id_usuario', 'id_categoria', 'ubicacion', 'precio', 'fecha', 'descripcion', 'categoria')

# Serializador para veterinarias y sus calificaciones

class VetSerializer(serializers.HyperlinkedModelSerializer):
    ubicacion = UbicacionSerializer()
    class Meta:
        model = Veterinaria
        fields = ('id', 'nombre', 'puntaje', 'horario', 'nombre_ubicacion', 'ubicacion')

class CalifVetSerializer(serializers.ModelSerializer):
    id_usuario = serializers.CharField(source='usuario_id')
    id_veterinaria = serializers.CharField(source='veterinaria_id')
    class Meta:
        model = Calificacion
        fields = ('id_usuario', 'id_veterinaria', 'puntaje', 'comentario')

# Serializadores para busquedas

class BusquedaSerializer(serializers.HyperlinkedModelSerializer):
    ubicacion = UbicacionSerializer()
    id_usuario = serializers.CharField(source='usuario_id')
    id_raza = serializers.CharField(source='raza_id')
    class Meta:
        model = Busqueda
        fields = ('id', 'nombre', 'id_usuario', 'ubicacion', 'tamanio', 'id_raza', 'edad', 'color')


# Serializadores para hallazgos

class HallazgoSerializer(serializers.HyperlinkedModelSerializer):
    ubicacion = UbicacionSerializer()
    id_usuario = serializers.CharField(source='usuario_id')
    id_raza = serializers.CharField(source='raza_id')
    fotos = serializers.RelatedField(many=True)
    class Meta:
        model = Hallazgo
        fields = ('id', 'nombre', 'tamanio', 'ubicacion', 'id_usuario',  'color', 'id_raza', 'edad', 'fotos')

# Serializadores para cruzas
#Crear aviso
class CruzaSerializer(serializers.HyperlinkedModelSerializer):
    ubicacion = UbicacionSerializer()
    id_usuario = serializers.CharField(source='usuario_id')
    id_raza = serializers.CharField(source='raza_id')
    fotos = serializers.RelatedField(many=True)
    class Meta:
        model = Cruza
        fields = ('id', 'nombre', 'id_usuario', 'id_raza', 'color', 'tamanio', 'edad', 'sexo', 'papeles', 'libreta', 'ubicacion', 'fotos')

#Crear solicitud de cruza
class SolicitudCruzaSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolicitudCruza
        fields = ('solicitante', 'solicitado')

# Serializadores para adopciones
class AdopcionSerializer(serializers.HyperlinkedModelSerializer):
    ubicacion = UbicacionSerializer()
    id_usuario = serializers.CharField(source='usuario_id')
    id_raza = serializers.CharField(source='raza_id')
    fotos = serializers.RelatedField(many=True)
    class Meta:
        model = Adopcion
        fields = ('id', 'nombre', 'id_usuario', 'id_raza', 'color', 'tamanio', 'edad', 'sexo', 'papeles', 'libreta', 'ubicacion', 'fotos')

#Crear solicitud de adopcion
class SolicitudAdopcionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolicitudAdopcion
        fields = ('solicitante', 'solicitado')

class SolicitudesAdopcionSerializer(serializers.ModelSerializer):
    id_solicitud = serializers.CharField(source='id')
    id_usuario = serializers.CharField(source='solicitante.id')
    nombre = serializers.CharField(source='solicitante.first_name')

    class Meta:
        model = SolicitudAdopcion

        fields = ('id', 'id_usuario', 'nombre')

class SolicitudesCruzaSerializer(serializers.ModelSerializer):
    id_usuario = serializers.CharField(source='solicitante.usuario.id')
    nombre = serializers.CharField(source='solicitante.usuario.first_name')

    class Meta:
        model = SolicitudCruza

        fields = ('id', 'id_usuario', 'nombre')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ('id', 'username', 'first_name', 'last_name', 'email', 'password')


#Imagenes

class FotoHallazgoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FotoHallazgo
        fields = ('hallazgo', 'imagen', 'descripcion')

class FotoAdopcionSerializer(serializers.ModelSerializer):
    class Meta:
        model = FotoAdopcion
        fields = ('adopcion', 'imagen', 'descripcion')

class FotoCruzaSerializer(serializers.ModelSerializer):
    class Meta:
        model = FotoCruza
        fields = ('cruza', 'imagen', 'descripcion')

class FotoSerializer(serializers.ModelSerializer):
    #imagen = serializers.Field('imagen.url')

    class Meta:
        model = Foto
        fields = ('id', 'imagen', 'descripcion')

#Mensajes
class MensajeSerializer(serializers.ModelSerializer):
    nombre = serializers.CharField(source='usuario_origen.first_name', required=False)
    class Meta:
        model = Mensaje
        fields = ('id', 'titulo', 'mensaje', 'modulo', 'usuario_origen', 'usuario_destino', 'fecha', 'leido', 'reference_id', 'nombre')
