OPCIONES_RAZAS = (
    ('AM', 'Alaskan Malamute'),
    ('BE', 'Beagle'),
    ('BI', 'Bichon Frise'),
    ('CA', 'Caniche'),
    ('CH', 'Chihuahua'),
)
OPCIONES_TAMANIO = (
    ('PE', 'Chico'),
    ('ME', 'Mediano'),
    ('GR', 'Grande'),
)
OPCIONES_COLOR = (
    ('C1', 'Color 1'),
    ('C2', 'Color 2'),
)
OPCIONES_SEXO = (
    ('M', 'Masculino'),
    ('F', 'Femenino'),
)