# Create your views here.

import os
from django.http import HttpResponse
from backend.models import *
from backend.serializers import *
from rest_framework import views
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
import math
from django.core.servers.basehttp import FileWrapper
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist

from django.db.models import Q

from django.contrib.auth.models import User

from rest_framework.parsers import FileUploadParser

class Ubicacion():
    lat = 0.0
    lon = 0.0
    rad = 50 # radio del circulo en km

    R = 6371.0;  # radio de la Tierra en km
    """
    # first-cut bounding box (in degrees)
    maxLat = lat + math.degrees(rad/R)
    minLat = lat - math.degrees(rad/R)
    # compensate for degrees longitude getting smaller with increasing latitude
    maxLon = lon + math.degrees(rad/R/math.cos(math.radians(lat)))
    minLon = lon - math.degrees(rad/R/math.cos(math.radians(lat)))
    """
    def get_min_lat(self):
        minLat = float(self.lat) - math.degrees(self.rad/self.R)
        return minLat
    def get_max_lat(self):
        maxLat = float(self.lat) + math.degrees(self.rad/self.R)
        return maxLat
    def get_min_lon(self):
        minLon = float(self.lon) - math.degrees(self.rad/self.R/math.cos(math.radians(float(self.lat))))
        return minLon
    def get_max_lon(self):
        maxLon = float(self.lon) + math.degrees(self.rad/self.R/math.cos(math.radians(float(self.lat))))
        return maxLon

def get_page(data, name, n_page):
    paginator = Paginator(data, 20)
    try:
        page = paginator.page(n_page)
        resp = {name:page.object_list}
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page = paginator.page(1)
        resp = {name:page.object_list}
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page = None
        resp = {name:{}}
    return resp

class VetList(generics.ListCreateAPIView):
    serializer_class = VetSerializer
    def get(self, request, format=None):
        queryset = self.get_queryset()
        data = VetSerializer(self.get_queryset(), many=True).data
        resp = {"veterinarias":data}
        n_page = self.request.QUERY_PARAMS.get('page', None)
        if n_page is not None:
            resp = get_page(data, "veterinarias", n_page)

        return Response(resp)

    def get_queryset(self):
        vet_filter = {}
        queryset = Veterinaria.objects.all()
        nombre = self.request.QUERY_PARAMS.get('nombre', None)
        lat = self.request.QUERY_PARAMS.get('lat', None)
        lon = self.request.QUERY_PARAMS.get('lon', None)
        if nombre is not None:
            vet_filter['nombre'] = nombre
        if (lat is not None) and (lon is not None):
            ubicacion = Ubicacion()
            ubicacion.lat = lat
            ubicacion.lon = lon
            vet_filter['ubicacion__lat__gte'] = ubicacion.get_min_lat()
            vet_filter['ubicacion__lat__lte'] = ubicacion.get_max_lat()
            vet_filter['ubicacion__lon__gte'] = ubicacion.get_min_lon()
            vet_filter['ubicacion__lon__lte'] = ubicacion.get_max_lon()
        queryset = queryset.filter(**vet_filter)
        return queryset


class VetDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Veterinaria.objects.all()
    serializer_class = VetSerializer

class CalifVetList(generics.ListCreateAPIView):
    queryset = Calificacion.objects.all()
    serializer_class = CalifVetSerializer

# Clasificados

class CategClasifList(generics.ListCreateAPIView):
    serializer_class = CategClasifSerializer
    def get(self, request, format=None):
        categs = CategoriasClasificado.objects.all()
        serializer = CategClasifSerializer(categs, many=True)
        n_page = self.request.QUERY_PARAMS.get('page', None)
        resp = {"categories":serializer.data}
        if n_page is not None:
            resp = get_page(serializer.data, "categories", n_page)
        return Response(resp)


class ClasifList(generics.ListCreateAPIView):
    serializer_class = ClasifSerializer
    def get(self, request, format=None):
        queryset = self.get_queryset()
        data = ClasifSerializer(self.get_queryset(), many=True).data
        resp = {"clasificados":data}
        n_page = self.request.QUERY_PARAMS.get('page', None)
        if n_page is not None:
            resp = get_page(data, "clasificados", n_page)

        return Response(resp)


    def get_queryset(self):
        clasif_filter = {}
        queryset = Clasificado.objects.all()
        categoria = self.request.QUERY_PARAMS.get('categoria', None)
        lat = self.request.QUERY_PARAMS.get('lat', None)
        lon = self.request.QUERY_PARAMS.get('lon', None)
        if categoria is not None:
            clasif_filter['categoria__id'] = categoria
        if (lat is not None) and (lon is not None):
            ubicacion = Ubicacion()
            ubicacion.lat = lat
            ubicacion.lon = lon
            clasif_filter['ubicacion__lat__gte'] = ubicacion.get_min_lat()
            clasif_filter['ubicacion__lat__lte'] = ubicacion.get_max_lat()
            clasif_filter['ubicacion__lon__gte'] = ubicacion.get_min_lon()
            clasif_filter['ubicacion__lon__lte'] = ubicacion.get_max_lon()
        queryset = queryset.filter(**clasif_filter)
        return queryset

class ClasifDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Clasificado.objects.all()
    serializer_class = ClasifSerializer

#Busquedas perdidos
class BusquedaList(generics.ListCreateAPIView):
    serializer_class = BusquedaSerializer
    def get(self, request, format=None):
        busquedas = self.get_queryset()
        serializer = BusquedaSerializer(busquedas, many=True)
        return Response({"busquedas":serializer.data})
    def get_queryset(self):
        busqueda_filter = {}
        queryset = Busqueda.objects.all()
        usuario = self.request.QUERY_PARAMS.get('usuario', None)
        if usuario is not None:
            busqueda_filter['usuario_id'] = usuario
        queryset = queryset.filter(**busqueda_filter)
        return queryset

#Manipular hallazgos
class HallazgoList(generics.ListCreateAPIView):
    serializer_class = HallazgoSerializer

    def get(self, request, format=None):
        data = HallazgoSerializer(self.get_queryset(), many=True).data
        resp = {"hallazgos":data}
        n_page = self.request.QUERY_PARAMS.get('page', None)
        if n_page is not None:
            resp = get_page(data, "hallazgos", n_page)
        return Response(resp)

    def post_save(self, obj, created=False):
        busquedas = Busqueda.objects.filter(raza=obj.raza, color=obj.color, edad=obj.edad,
                tamanio=obj.tamanio)

        for busqueda in busquedas:
            mensaje = "Se ha encontrado un perro que podria ser el tuyo"
            solicitante = busqueda.usuario
            enviar_mensaje('Hallazgo reportado', mensaje, 'H', solicitante, solicitante, obj.id)

    def get_queryset(self):
        hallazgo_filter = []
        queryset = Hallazgo.objects.all()
        lat = self.request.QUERY_PARAMS.get('lat', None)
        lon = self.request.QUERY_PARAMS.get('lon', None)
        tamanio = self.request.QUERY_PARAMS.get('tamanio', None)
        raza = self.request.QUERY_PARAMS.get('raza', None)
        color = self.request.QUERY_PARAMS.get('color', None)
        edad = self.request.QUERY_PARAMS.get('edad', None)
        sexo = self.request.QUERY_PARAMS.get('sexo', None)
        if (lat is not None) and (lon is not None):
            ubicacion = Ubicacion()
            ubicacion.lat = lat
            ubicacion.lon = lon
            hallazgo_filter.append(Q(ubicacion__lat__gte=ubicacion.get_min_lat()))
            hallazgo_filter.append(Q(ubicacion__lat__lte=ubicacion.get_max_lat()))
            hallazgo_filter.append(Q(ubicacion__lon__gte=ubicacion.get_min_lon()))
            hallazgo_filter.append(Q(ubicacion__lon__lte=ubicacion.get_max_lon()))
        if tamanio is not None and tamanio != 'D':
            hallazgo_filter.append((Q(tamanio=tamanio) | Q(tamanio='D')))
        if raza is not None and int(raza) != 1:
            hallazgo_filter.append((Q(raza=Raza.objects.get(pk=raza))) | Q(raza=Raza.objects.get(pk=1)))
        if color is not None and color != 'D':
            hallazgo_filter.append((Q(color=color) | Q(color='D')))
        if edad is not None and edad != 'D':
            hallazgo_filter.append((Q(edad=edad) | Q(edad='D')))
        if sexo is not None and sexo != 'D':
            hallazgo_filter.append((Q(sexo=sexo) | Q(sexo='D')))
        queryset = queryset.filter(*hallazgo_filter).order_by('pk')
        return queryset

#Cruza
class CruzaList(generics.ListCreateAPIView):
    serializer_class = CruzaSerializer
    def get(self, request, format=None):
        data = CruzaSerializer(self.get_queryset(), many=True).data
        resp = {"avisos":data}
        n_page = self.request.QUERY_PARAMS.get('page', None)
        if n_page is not None:
            resp = get_page(data, "avisos", n_page)
        return Response(resp)

    def get_queryset(self):
        cruza_filter = []
        queryset = Cruza.objects.all()
        lat = self.request.QUERY_PARAMS.get('lat', None)
        lon = self.request.QUERY_PARAMS.get('lon', None)
        tamanio = self.request.QUERY_PARAMS.get('tamanio', None)
        raza = self.request.QUERY_PARAMS.get('raza', None)
        color = self.request.QUERY_PARAMS.get('color', None)
        edad = self.request.QUERY_PARAMS.get('edad', None)
        sexo = self.request.QUERY_PARAMS.get('sexo', None)
        papeles = self.request.QUERY_PARAMS.get('papeles', None)
        libreta = self.request.QUERY_PARAMS.get('vacunas', None)
        usuario = self.request.QUERY_PARAMS.get('usuario', None)
        if (lat is not None) and (lon is not None):
            ubicacion = Ubicacion()
            ubicacion.lat = lat
            ubicacion.lon = lon
            cruza_filter.append(Q(ubicacion__lat__gte=ubicacion.get_min_lat()))
            cruza_filter.append(Q(ubicacion__lat__lte=ubicacion.get_max_lat()))
            cruza_filter.append(Q(ubicacion__lon__gte=ubicacion.get_min_lon()))
            cruza_filter.append(Q(ubicacion__lon__lte=ubicacion.get_max_lon()))
        if tamanio is not None and tamanio != 'D':
            cruza_filter.append((Q(tamanio=tamanio) | Q(tamanio='D')))
        if raza is not None and int(raza) != 1:
            cruza_filter.append((Q(raza=Raza.objects.get(pk=raza)) | Q(raza=Raza.objects.get(pk=1))))
        if color is not None and color != 'D':
            cruza_filter.append((Q(color=color) | Q(color='D')))
        if edad is not None and edad != 'D':
            cruza_filter.append((Q(edad=edad) | Q(edad='D')))
        if sexo is not None and sexo != 'D':
            cruza_filter.append((Q(sexo=sexo) | Q(sexo='D')))
        if papeles is not None:
            if papeles.lower() == 'true':
                cruza_filter.append(Q(papeles=True))
        if libreta is not None:
            if libreta.lower() == 'true':
                cruza_filter.append(Q(libreta=True))
        queryset = queryset.filter(*cruza_filter).order_by('-pk')
        if usuario is not None:
            solicitudes_usuario = SolicitudCruza.objects.filter(solicitante__usuario = usuario).values('solicitado__id')
            avisos_usuario = Cruza.objects.filter(usuario__id = usuario).values('id')
            queryset = queryset.exclude(id__in = solicitudes_usuario)
            queryset = queryset.exclude(id__in = avisos_usuario)

        return queryset

class CruzaUsuarioList(generics.ListCreateAPIView):
    serializer_class = CruzaSerializer
    def get(self, request, usuario_id, format=None):
        try:
            #Se muestran solo los avisos que no tengan una solicitud aceptada
            solicitudes_aceptadas = SolicitudCruza.objects.filter(estado = 'A').values('solicitado__id')
            cruzas = Cruza.objects.filter(usuario=usuario_id).exclude(id__in = solicitudes_aceptadas)
            data = CruzaSerializer(cruzas, many=True).data
        except ObjectDoesNotExist:
            data = {}
        resp = {"avisos":data}
        return Response(resp)

class CruzaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cruza.objects.all()
    serializer_class = CruzaSerializer

class HallazgoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Hallazgo.objects.all()
    serializer_class = HallazgoSerializer

class SolicitudCruzaList(generics.ListCreateAPIView):
    queryset = SolicitudCruza.objects.all()
    serializer_class = SolicitudCruzaSerializer

class SolicitudesCruzaList(generics.ListCreateAPIView):
    serializer_class = SolicitudesCruzaSerializer
    def get(self, request, cruza_id, format=None):
        try:
            data = SolicitudesCruzaSerializer(SolicitudCruza.objects.filter(solicitado_id = cruza_id, estado = 'P'), many=True).data
        except ObjectDoesNotExist:
            data = {}
        resp = {"solicitudes":data}
        return Response(resp)

#Adopcion
class AdopcionList(generics.ListCreateAPIView):
    serializer_class = AdopcionSerializer
    def get(self, request, format=None):
        data = AdopcionSerializer(self.get_queryset(), many=True).data
        resp = {"avisos":data}
        n_page = self.request.QUERY_PARAMS.get('page', None)
        if n_page is not None:
            resp = get_page(data, "avisos", n_page)
        return Response(resp)
    def get_queryset(self):
        adopcion_filter = []
        queryset = Adopcion.objects.all()
        lat = self.request.QUERY_PARAMS.get('lat', None)
        lon = self.request.QUERY_PARAMS.get('lon', None)
        tamanio = self.request.QUERY_PARAMS.get('tamanio', None)
        raza = self.request.QUERY_PARAMS.get('raza', None)
        color = self.request.QUERY_PARAMS.get('color', None)
        edad = self.request.QUERY_PARAMS.get('edad', None)
        sexo = self.request.QUERY_PARAMS.get('sexo', None)
        papeles = self.request.QUERY_PARAMS.get('papeles', None)
        libreta = self.request.QUERY_PARAMS.get('vacunas', None)
        usuario = self.request.QUERY_PARAMS.get('usuario', None)
        if (lat is not None) and (lon is not None):
            ubicacion = Ubicacion()
            ubicacion.lat = lat
            ubicacion.lon = lon
            adopcion_filter.append(Q(ubicacion__lat__gte=ubicacion.get_min_lat()))
            adopcion_filter.append(Q(ubicacion__lat__lte=ubicacion.get_max_lat()))
            adopcion_filter.append(Q(ubicacion__lon__gte=ubicacion.get_min_lon()))
            adopcion_filter.append(Q(ubicacion__lon__lte=ubicacion.get_max_lon()))
        if tamanio is not None and tamanio != 'D':
            adopcion_filter.append((Q(tamanio=tamanio) | Q(tamanio='D')))
        if raza is not None and int(raza) != 1 :
            adopcion_filter.append((Q(raza=Raza.objects.get(pk=raza))) | Q(raza=Raza.objects.get(pk=1)))
        if color is not None and color != 'D':
            adopcion_filter.append((Q(color=color) | Q(color='D')))
        if edad is not None and edad != 'D':
            adopcion_filter.append((Q(edad=edad) | Q(edad='D')))
        if sexo is not None and sexo != 'D':
            adopcion_filter.append((Q(sexo=sexo) | Q(sexo='D')))
        if papeles is not None:
            if papeles.lower() == 'true':
                adopcion_filter.append(Q(papeles=True))
        if libreta is not None:
            if libreta.lower() == 'true':
                adopcion_filter.append(Q(libreta=True))
        queryset = queryset.filter(*adopcion_filter).order_by('pk')
        if usuario is not None:
            solicitudes_usuario = SolicitudAdopcion.objects.filter(solicitante__id = usuario).values('solicitado__id')
            avisos_usuario = Adopcion.objects.filter(usuario__id = usuario).values('id')
            queryset = queryset.exclude(id__in = solicitudes_usuario)
            queryset = queryset.exclude(id__in = avisos_usuario)
        return queryset

class AdopcionUsuarioList(generics.ListCreateAPIView):
    serializer_class = AdopcionSerializer
    def get(self, request, usuario_id, format=None):
        try:
            #Se muestran solo los avisos que no tengan una solicitud aceptada
            solicitudes_aceptadas = SolicitudAdopcion.objects.filter(estado = 'A').values('solicitado__id')
            adopciones = Adopcion.objects.filter(usuario=usuario_id).exclude(id__in = solicitudes_aceptadas)
            data = AdopcionSerializer(adopciones, many=True).data
        except ObjectDoesNotExist:
            data = {}
        resp = {"avisos":data}
        return Response(resp)

class BusquedaUsuarioList(generics.ListCreateAPIView):
    serializer_class = BusquedaSerializer
    def get(self, request, usuario_id, format=None):
        try:
            data = BusquedaSerializer(Busqueda.objects.filter(usuario=usuario_id), many=True).data
        except ObjectDoesNotExist:
            data = {}
        resp = {"busquedas": data}
        return Response(resp)

class AdopcionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Adopcion.objects.all()
    serializer_class = AdopcionSerializer

class SolicitudAdopcionList(generics.ListCreateAPIView):
    queryset = SolicitudAdopcion.objects.all()
    serializer_class = SolicitudAdopcionSerializer

class SolicitudesAdopcionList(generics.ListCreateAPIView):
    serializer_class = SolicitudesAdopcionSerializer
    def get(self, request, adopcion_id, format=None):
        try:
            data = SolicitudesAdopcionSerializer(SolicitudAdopcion.objects.filter(solicitado_id = adopcion_id, estado = 'P'), many=True).data
        except ObjectDoesNotExist:
            data = {}
        resp = {"solicitudes":data}
        return Response(resp)

def rechazar_solicitud_cruza(request, solicitud_id):
    s = SolicitudCruza.objects.get(pk=solicitud_id)
    s.estado = 'R'
    s.save()
    solicitante = s.solicitante.usuario
    nombrePerro = s.solicitado.nombre
    mensaje="Tu solicitud de cruza del perro " + nombrePerro + " ha sido rechazada."
    enviar_mensaje('Solicitud de cruza rechazada', mensaje, 'A', solicitante , solicitante, s.solicitado.id)
    return HttpResponse(status=200)

def confirmar_solicitud_cruza(request, solicitud_id):
    s = SolicitudCruza.objects.get(pk=solicitud_id)
    s.estado = 'A'
    s.save()
    solicitudes_rechazadas = SolicitudCruza.objects.filter(solicitado__id = s.solicitado.id).exclude(id = s.pk)
    solicitudes_rechazadas.update(estado = 'R')
    for solicitud in solicitudes_rechazadas:
        solicitante = solicitud.solicitante.usuario
        nombrePerro = solicitud.solicitado.nombre
        mensaje="Tu solicitud de cruza del perro " + nombrePerro + " ha sido rechazada por la aceptacion de otra solicitud."
        enviar_mensaje('Solicitud de cruza rechazada', mensaje, 'A', solicitante, solicitante, solicitud.solicitado.id)
    return HttpResponse(status=200)

def rechazar_solicitud_adopcion(request, solicitud_id):
    s = SolicitudAdopcion.objects.get(pk=solicitud_id)
    s.estado = 'R'
    s.save()
    solicitante = s.solicitante
    nombrePerro = s.solicitado.nombre
    mensaje="Tu solicitud de adopcion del perro " + nombrePerro + " ha sido rechazada."
    enviar_mensaje('Solicitud de adopcion rechazada', mensaje, 'A', solicitante, solicitante, s.solicitado.id)
    return HttpResponse(status=200)

def confirmar_solicitud_adopcion(request, solicitud_id):
    s = SolicitudAdopcion.objects.get(pk=solicitud_id)
    s.estado = 'A'
    s.save()
    solicitudes_rechazadas = SolicitudAdopcion.objects.filter(solicitado__id = s.solicitado.id).exclude(id = s.pk)
    solicitudes_rechazadas.update(estado = 'R')
    for solicitud in solicitudes_rechazadas:
        solicitante = solicitud.solicitante
        nombrePerro = solicitud.solicitado.nombre
        mensaje="Tu solicitud de adopcion del perro " + nombrePerro + " ha sido rechazada por la aceptacion de otra solicitud."
        enviar_mensaje('Solicitud de adopcion rechazada', mensaje, 'A', User.objects.get(pk=1), solicitante, solicitud.solicitado.id)

    return HttpResponse(status=200)

def enviar_mensaje(titulo, texto, modulo, usuario_orig, usuario_dest, reference_id):
    message = Mensaje(titulo=titulo,
                          mensaje=texto,
                          modulo=modulo,
                          usuario_origen=usuario_orig,
                          usuario_destino=usuario_dest,
                          reference_id=reference_id)
    message.save()

class UserList(generics.ListCreateAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UserSerializer

    def get_queryset(self):
        username = self.request.QUERY_PARAMS.get('username', None)
        password = self.request.QUERY_PARAMS.get('password', None)
        if password and username:
            return Usuario.objects.filter(username=username, password=password)
        else:
            return Usuario.objects.all()


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UserSerializer

class FotoList(generics.ListCreateAPIView):
    model = Foto
    serializer_class = FotoSerializer

def send_image(request, image_id):
    image = Foto.objects.get(id = image_id);
    image_data = open(image.imagen.path, "rb").read()
    response = HttpResponse(image_data, content_type='image/jpeg')
    return response

class FotoHallazgoList(generics.ListCreateAPIView):
    model = FotoHallazgo
    serializer_class = FotoHallazgoSerializer

class FotoAdopcionList(generics.ListCreateAPIView):
    model = FotoAdopcion
    serializer_class = FotoAdopcionSerializer

class FotoCruzaList(generics.ListCreateAPIView):
    model = FotoCruza
    serializer_class = FotoCruzaSerializer

def send_image_hallazgo(request, image_id):
    image = FotoHallazgo.objects.get(id = image_id);
    image_data = open(image.imagen.path, "rb").read()
    response = HttpResponse(image_data, content_type='image/jpeg')
    return response

def send_image_adopcion(request, image_id):
    image = FotoAdopcion.objects.get(id = image_id);
    image_data = open(image.imagen.path, "rb").read()
    response = HttpResponse(image_data, content_type='image/jpeg')
    return response

def send_image_cruza(request, image_id):
    image = FotoCruza.objects.get(id = image_id);
    image_data = open(image.imagen.path, "rb").read()
    response = HttpResponse(image_data, content_type='image/jpeg')
    return response

class RazaList(generics.ListCreateAPIView):
    model = Raza
    serializer_class = RazaSerializer

    def get(self, request, format=None):
        queryset = self.get_queryset()
        data = RazaSerializer(Raza.objects.all(), many=True).data
        resp = {"razas":data}
        n_page = self.request.QUERY_PARAMS.get('page', None)
        if n_page is not None:
            resp = get_page(data, "razas", n_page)

        return Response(resp)

#Mensajes
class MensajeList(generics.ListCreateAPIView):
    model = Mensaje
    serializer_class = MensajeSerializer


class MensajeUsuarioList(generics.ListCreateAPIView):
    model = Mensaje
    serializer_class = MensajeSerializer
    def get(self, request, usuario_id, format=None):
        data = MensajeSerializer(self.get_resultados(usuario_id), many=True).data
        resp = {"mensajes":data}
        return Response(resp)

    def get_resultados(self, usuario_id):
        filter = {}
        queryset = Mensaje.objects.filter(usuario_destino__id = usuario_id)
        leido = self.request.QUERY_PARAMS.get('leido', None)
        modulo = self.request.QUERY_PARAMS.get('modulo', None)
        if modulo is not None:
            filter['modulo'] = modulo
        if leido is not None:
            if leido.lower() == 'true':
                filter['leido'] = True
            else:
                filter['leido'] = False
        queryset = queryset.filter(**filter)
        return queryset

class MensajeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Mensaje.objects.all()
    serializer_class = MensajeSerializer
