from django.conf.urls import patterns, include, url
import sys
sys.path.append('./backend')
from backend import views
from frontend import views as front_views

from django.contrib import admin
admin.autodiscover()

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'huesitos.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r"^$", "main"),
    #Urls del front
    url(r"^$", front_views.home),
    url(r"^veterinarias/$", front_views.veterinarias),
    url(r"^clasificados/$", front_views.clasificados),
    url(r"^clasificados/nuevo$", front_views.nuevo_clasificado),
    url(r"^adopcion/$", front_views.adopcion),
    url(r"^adoptar/$", front_views.adoptar),
    url(r"^busqueda/$", front_views.busqueda),
    url(r"^hallazgo/$", front_views.hallazgo),
    url(r"^cruza/$", front_views.cruza),
    url(r"^cruza/nueva$", front_views.nueva_cruza),
    url(r"^mis_avisos/$", front_views.perfil),
    url(r"^mis_mensajes/$", front_views.mensajes),
    url(r"^veterinarias/$", front_views.veterinarias),
    url(r"^signin/$", front_views.signin),
    url(r"^signup/$", front_views.signup),

    #Admin
    url(r'^admin/', include(admin.site.urls)),

    #API
    url(r'^api/veterinarias/$', views.VetList.as_view()),
    url(r'^api/veterinarias/(?P<pk>[0-9]+)/$', views.VetDetail.as_view()),
    url(r'^api/veterinarias/calificar/$', views.CalifVetList.as_view()),
    url(r'^api/clasificados/$', views.ClasifList.as_view()),
    url(r'^api/clasificados/(?P<pk>[0-9]+)/$', views.VetDetail.as_view()),
    url(r'^api/clasificados/categorias/$', views.CategClasifList.as_view()),
    url(r'^api/busqueda/$', views.BusquedaList.as_view()),
    url(r'^api/hallazgos/$', views.HallazgoList.as_view()),
    url(r'^api/hallazgo/(?P<pk>[0-9]+)/$', views.HallazgoDetail.as_view()),
    url(r'^api/cruza/$', views.CruzaList.as_view()),
    url(r'^api/cruza/(?P<pk>[0-9]+)/$', views.CruzaDetail.as_view()),
    url(r'^api/cruza/(?P<cruza_id>\d+)/solicitar/$', views.SolicitudCruzaList.as_view()),
    url(r'^api/cruza/solicitud/(?P<solicitud_id>[0-9]+)/rechazar/$', views.rechazar_solicitud_cruza, name='rechazar_solicitud_cruza'),
    url(r'^api/cruza/solicitud/(?P<solicitud_id>[0-9]+)/confirmar/$', views.confirmar_solicitud_cruza, name='confirmar_solicitud_cruza'),

    url(r'^api/user/(?P<usuario_id>[0-9]+)/cruza/$', views.CruzaUsuarioList.as_view()),
    url(r'^api/cruza/(?P<cruza_id>[0-9]+)/solicitudes/$', views.SolicitudesCruzaList.as_view()),

    url(r'^api/adopcion/$', views.AdopcionList.as_view()),
    url(r'^api/adopcion/(?P<pk>[0-9]+)/$', views.AdopcionDetail.as_view()),
    url(r'^api/adopcion/(?P<adopcion_id>\d+)/solicitar/$', views.SolicitudAdopcionList.as_view()),
    url(r'^api/adopcion/solicitud/(?P<solicitud_id>[0-9]+)/rechazar/$', views.rechazar_solicitud_adopcion, name='rechazar_solicitud_adopcion'),
    url(r'^api/adopcion/solicitud/(?P<solicitud_id>[0-9]+)/confirmar/$', views.confirmar_solicitud_adopcion, name='confirmar_solicitud_adopcion'),
    url(r'^api/adopcion/(?P<adopcion_id>[0-9]+)/solicitudes/$', views.SolicitudesAdopcionList.as_view()),
    url(r'^api/user/(?P<usuario_id>[0-9]+)/adopcion/$', views.AdopcionUsuarioList.as_view()),
    url(r'^api/user/(?P<usuario_id>[0-9]+)/busqueda/$', views.BusquedaUsuarioList.as_view()),
    url(r'^api/users/$', views.UserList.as_view(), name='user_list'),
    url(r'^api/users/(?P<pk>[0-9]+)$', views.UserDetail.as_view(), name='user_details'),
    url(r'^api/api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    #url(r'^api/imagenes/(?P<pk>\d+)$', views.PhotoDetail.as_view(), name='photo-detail'),
    url(r'^api/imagenes/(?P<image_id>\d+)$', views.send_image, name='send_image'),
    url(r'^api/imagenes/$', views.FotoList.as_view()),
    url(r'^api/imagenes/hallazgo/$', views.FotoHallazgoList.as_view()),
    url(r'^api/imagenes/cruza/$', views.FotoCruzaList.as_view()),
    url(r'^api/imagenes/adopcion/$', views.FotoAdopcionList.as_view()),
    url(r'^api/imagenes/hallazgo/(?P<image_id>\d+)/$', views.send_image_hallazgo, name='send_image_hallazgo'),
    url(r'^api/imagenes/cruza/(?P<image_id>\d+)/$', views.send_image_cruza, name='send_image_cruza'),
    url(r'^api/imagenes/adopcion/(?P<image_id>\d+)/$', views.send_image_adopcion, name='send_image_adopcion'),

    url(r'^api/razas/$', views.RazaList.as_view()),

    #Mensajes
    url(r'^api/mensaje/$', views.MensajeList.as_view()),
    url(r'^api/mensaje/(?P<pk>[0-9]+)/$', views.MensajeDetail.as_view()),
    url(r'^api/user/(?P<usuario_id>[0-9]+)/mensajes/$', views.MensajeUsuarioList.as_view())
)


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

